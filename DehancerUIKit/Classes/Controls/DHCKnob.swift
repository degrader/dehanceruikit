//
//  DHCCustomKnob.swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 22.09.17.
//  Copyright © 2017 Dehancer. All rights reserved.
//

import Cocoa

@IBDesignable public class DHCKnob: DHCView {
    public override init(frame frameRect: NSRect) { super.init(frame: frameRect)}    
    public required init?(coder decoder: NSCoder) {super.init(coder: decoder)}
}
