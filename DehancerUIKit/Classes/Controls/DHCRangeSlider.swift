//
//  DHCRangeSlider.swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 22.09.17.
//  Copyright © 2017 Dehancer. All rights reserved.
//

import Cocoa
/// RangeSlider delegate protocol
@objc public protocol DHCRangeSliderDelegate {
    
    /// Range slider action handler is invoked when slider values is changed
    ///
    /// - Parameters:
    ///   - control: the current RangeSlider object
    ///   - start: start value
    ///   - end: end value
    func rangeSlider(_ control: DHCRangeSlider, didChange start: CGFloat,      end: CGFloat)

    
    /// Range slider action handler is invoked when slider values is stopped changing
    ///
    /// - Parameters:
    ///   - control: the current RangeSlider object
    ///   - start: start value
    ///   - end: end value
    @objc optional func rangeSlider(_ control: DHCRangeSlider, didEndChanging start:CGFloat, end:CGFloat)
    
    
    /// Range slider action handler is invoked when slider values is started changing
    ///
    /// - Parameters:
    ///   - control: the current RangeSlider object
    ///   - start: start value
    ///   - end: end value
    @objc optional func rangeSlider(_ control: DHCRangeSlider, willBeginChanging start:CGFloat, end:CGFloat)
}




/// Range Slider
@IBDesignable public class DHCRangeSlider: NSControl {
        
    @IBInspectable public var fillColor: NSColor  = NSColor.white {didSet { needsDisplay = true }}
    @IBInspectable public var trackColor: NSColor = NSColor.gray {didSet { needsDisplay = true }}
    @IBInspectable public var knobColor: NSColor  = NSColor.white {
        didSet { 
            startKnob.backgroundColor = knobColor
            endKnob.backgroundColor = knobColor
            startKnob.needsDisplay = true
            endKnob.needsDisplay = true
            needsDisplay = true
        }        
    }
    
    @IBInspectable public var textColor:NSColor            = NSColor.gray {
        didSet {
            startLabel.textColor = textColor
            endLabel.textColor = textColor
            needsDisplay = true
        }
    }
    
    @IBInspectable public var textFont:NSFont              = NSFont(name: "HelveticaNeue", size: 22)! {
        didSet {
            startLabel.font  = textFont
            endLabel.font  = textFont
            needsDisplay = true
        }
    }
    
    public var trackLabelformat:String = "%.0f" { didSet{ needsDisplay = true } }
    
    // TODO: Add option to hide info labels
    
    public var minValue: CGFloat                           = 0 
    public var maxValue: CGFloat                           = 100
    
    public var startValue:   CGFloat                       = 0  { 
        didSet{ if
            startValue  < minValue   || startValue  > endValue { startValue  = minValue };  
            needsDisplay = true
        }
    }
    public var endValue:  CGFloat                       = 100    { 
        didSet{ 
            if endValue < startValue || endValue > maxValue    { endValue = maxValue } ; 
            needsDisplay = true
        }
    }
    
    public var minimValue:CGFloat                          = 10
    
    public var delegate: DHCRangeSliderDelegate?
    
    // Custom Private Vars
    fileprivate var startKnob:DHCKnob    = DHCKnob()
    fileprivate var endKnob:DHCKnob     = DHCKnob()
    fileprivate lazy var startLabel:NSTextField =  {
        return NSTextField()
    }() 
    fileprivate lazy var endLabel:NSTextField      =  {
        return NSTextField()
    }() 
    
    fileprivate var yOrigin:CGFloat             = 0
    fileprivate var lineMaxWidh: CGFloat        = 0
    fileprivate var shouldMoveFirst:Bool        = false
    fileprivate var shouldMoveLast: Bool        = false
    
    override public init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        configure()  
        reDraw()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
     
    override open func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
    }
    
    var trackingArea:NSTrackingArea!
    override open func updateTrackingAreas() {
        if trackingArea != nil {
            self.removeTrackingArea(trackingArea)
        }
        trackingArea               = NSTrackingArea(rect: self.bounds, options: [.enabledDuringMouseDrag , .mouseMoved, .activeAlways, .cursorUpdate], owner: self, userInfo: nil)
        self.addTrackingArea(trackingArea)
    }
    
    
    /// Mouse down event : We test if current mouse location is inside of first or second knob. If yes, then we 
    // tell that selected knob that it can be moved
    override open func mouseDown(with theEvent: NSEvent) {        
        let loc = self.convert(theEvent.locationInWindow, from: self.window?.contentView)
        let firstFrame = NSInsetRect(startKnob.frame, -5, -5)
        let secFrame = NSInsetRect(endKnob.frame, -5, -5)
        
        if NSPointInRect(loc, firstFrame) {
            shouldMoveFirst = true
        }        
        if NSPointInRect(loc, secFrame) {
            shouldMoveLast = true
        }
        
        if let delegate = self.delegate {
            delegate.rangeSlider?(self, willBeginChanging: startValue, end: endValue)
        }
    }
    
    // Mouse dragged Event : if is any selected knob we will move to new position, and we calculate
    // new the new slider values
    override open func mouseDragged(with theEvent: NSEvent) {
        var loc = self.convert(theEvent.locationInWindow, from: self.window?.contentView)
        
        let _ = self.convert(theEvent.locationInWindow, to: self)
        
        if shouldMoveFirst {
            
            loc.x -= startKnob.frame.width/2
            
            let minim = CGFloat(0)
            let maxim   = endKnob.frame.origin.x - startKnob.frame.width - ((lineMaxWidh /  maxValue)  * minimValue )
            if loc.x > minim && loc.x < maxim {
                startValue = (loc.x * maxValue) / lineMaxWidh
                if loc.x < minim {
                    startValue = (minim * maxValue) / lineMaxWidh
                }
                if loc.x > maxim {
                    startValue = (maxim * maxValue) / lineMaxWidh
                }
            }else if loc.x < minim {
                startValue = (minim * maxValue) / lineMaxWidh
            }
            self.needsDisplay = true
            didChangeValuesHandler()
        }
        if shouldMoveLast {
            
            loc.x -= endKnob.frame.width/2
            
            let minim = startKnob.frame.origin.x + endKnob.frame.width + ((lineMaxWidh /  maxValue)  * minimValue )
            let maxim   = lineMaxWidh
            if loc.x > minim && loc.x < maxim {
                endValue = (loc.x * maxValue) / lineMaxWidh
                if loc.x < minim {
                    endValue = (minim * maxValue) / lineMaxWidh
                }
                if loc.x > maxim {
                    endValue = (maxim * maxValue) / lineMaxWidh
                }
            } else if loc.x > maxim  {
                endValue = (maxim * maxValue) / lineMaxWidh
            }
            self.needsDisplay = true
            didChangeValuesHandler()
        }
    }
    
    //Mouse up event : We "deselect" both knobs.
    override open func mouseUp(with theEvent: NSEvent) {
        shouldMoveLast  = false
        shouldMoveFirst = false
        if let delegate = self.delegate{
            delegate.rangeSlider?(self, didEndChanging: startValue, end: endValue)
        }
    }
    
    private func lableSize(_ value:CGFloat) -> NSSize {
        return self.valueToString(value).size(withAttributes: [NSAttributedStringKey.font : self.textFont ])
    }
    
    private func reDraw() {
        
        //let textOrigin: CGFloat = 0
        let knobSize            = self.frame.height * 0.4
        lineMaxWidh             = self.frame.width - knobSize
        yOrigin                 = self.frame.height * 0.6
        
        startLabel.stringValue   = valueToString(startValue)
        endLabel.stringValue  = valueToString(endValue)

        startLabel.sizeToFit()
        endLabel.sizeToFit()
        
        let firstX   = (startValue * lineMaxWidh) /  maxValue 
        let secondX  = (endValue * lineMaxWidh) / maxValue
        
        startKnob.setFrameSize(NSMakeSize(knobSize, knobSize))
        endKnob.setFrameSize(NSMakeSize(knobSize, knobSize))
        startKnob.setFrameOrigin(NSMakePoint(firstX, yOrigin))
        endKnob.setFrameOrigin(NSMakePoint(secondX, yOrigin))
        
        startLabel.frame.centerX = startKnob.frame.centerX
        endLabel.frame.centerX = endKnob.frame.centerX
        
        if endLabel.frame.x + endLabel.frame.width >= bounds.width {
            endLabel.frame.origin.x = bounds.width - endLabel.frame.width 
        }
        
        if startLabel.frame.x < 0 {
            startKnob.frame.origin.x = 0
        }
        
        // Draw  background line
        let backgroundLine              = NSBezierPath()
        backgroundLine.move(to: NSMakePoint(knobSize / 2,  self.frame.height * 0.8))
        backgroundLine.line(to: NSMakePoint(lineMaxWidh + knobSize / 2 ,  self.frame.height * 0.8))
        //backgroundLine.lineCapStyle     = NSBezierPath.LineCapStyle.roundLineCapStyle
        backgroundLine.lineWidth        = 3
        fillColor.set()
        backgroundLine.stroke()
        
        ///Draw selection  line (the line between knobs)
        let selectionLine               = NSBezierPath()
        selectionLine.move(to: NSMakePoint(firstX + knobSize / 2 , self.frame.height * 0.8))
        selectionLine.line(to: NSMakePoint(secondX + knobSize / 2 , self.frame.height * 0.8))
        selectionLine.lineCapStyle      = NSBezierPath.LineCapStyle.roundLineCapStyle
        selectionLine.lineWidth         = 3
        trackColor.setStroke()
        selectionLine.stroke()        
    }
    
    
    private func configure(){

        startKnob.backgroundColor = knobColor
        endKnob.backgroundColor = knobColor

        startLabel.isBordered       = false
        startLabel.identifier       = NSUserInterfaceItemIdentifier(rawValue: "10")
        startLabel.isEditable       = false
        startLabel.isSelectable     = false
        startLabel.stringValue      = valueToString(startValue)
        startLabel.backgroundColor  = NSColor.clear
        startLabel.font             = textFont
        startLabel.textColor        = textColor
        startLabel.alignment        = NSTextAlignment.center
        
        endLabel.isBordered      = false
        endLabel.identifier      = NSUserInterfaceItemIdentifier(rawValue: "10")
        endLabel.isEditable      = false
        endLabel.isSelectable    = false
        endLabel.stringValue     = valueToString(endValue)
        endLabel.backgroundColor = NSColor.clear
        endLabel.font            = textFont
        endLabel.textColor       = textColor
        endLabel.alignment       = NSTextAlignment.center
        
        self.addSubview(startKnob)
        self.addSubview(endKnob)
        self.addSubview(startLabel)
        self.addSubview(endLabel)
        
        needsDisplay = true
    }
    
    /// If has a delegate we will send  changed notification, and new values for slider.
    /// Also we trigger action if this control has one.
    private func didChangeValuesHandler() {
        if self.action != nil {
            NSApp.sendAction(self.action!, to: self.target, from: self)
        }
        
        if let delegate = self.delegate {
            delegate.rangeSlider(self, didChange: startValue, end: endValue)
        }
    }
    
    /// Convert seconds to 00:00 format
    fileprivate func valueToString(_ value:CGFloat) -> String{
        return String(format: trackLabelformat, value)
    }
}
