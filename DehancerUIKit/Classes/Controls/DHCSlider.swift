//
//  DHCSlider.swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 04.11.17.
//

import Cocoa

public class DHCSliderCell: NSSliderCell {
    
    fileprivate var knobImage:NSImage?             
    fileprivate var barFillImage:NSImage?          
    fileprivate var barFillBeforeKnobImage:NSImage? 
    fileprivate var barLeftAgeImage:NSImage?        
    fileprivate var barRightAgeImage:NSImage?
    
    private var _currentKnobRect:NSRect? 
    private var _barRect:NSRect!
    private var _flipped:Bool = false
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public init() {
        super.init()
    }
    
    public init(knob:NSImage, 
                barFill:NSImage?=nil, 
                barFillBeforeKnob:NSImage?=nil, 
                barLeftAge:NSImage?=nil, 
                barRightAge:NSImage?=nil) {
        
        knobImage = knob.flip(direction: 1);
        barFillImage = barFill;
        barFillBeforeKnobImage = barFillBeforeKnob;
        barLeftAgeImage = barLeftAge;
        barRightAgeImage = barRightAge;
        super.init()
        
    }
    
    override public func drawKnob(_ knobRect: NSRect) {        
       
        guard let knobImage = self.knobImage else {
            super.drawKnob(knobRect)
            return;            
        }        
        
        _currentKnobRect = knobRect;
        drawBar(inside: _barRect, flipped: _flipped)
                
        self.controlView?.lockFocus()
        
        //let newOriginX:CGFloat = knobRect.origin.x
        var rect = knobRect
        rect.origin.x  = (knobImage.size.width-knobRect.size.width)/2
        if let fill = barFillImage?.size {
            rect.origin.y -= fill.height/4
        }
        
        knobImage.draw(at: knobRect.origin, 
                       from: rect, 
                       operation: .sourceOver, fraction: 1)
        
        self.controlView?.unlockFocus()        
    }
    
    public override func draw(withFrame cellFrame: NSRect, in controlView: NSView) {
        super.draw(withFrame: cellFrame, in: controlView)
    }
    
    override public func drawBar(inside aRect: NSRect, flipped: Bool) {
        
        if( (knobImage == nil) && (barFillImage == nil) && (barFillBeforeKnobImage == nil) &&
            (barLeftAgeImage == nil) && (barRightAgeImage == nil) ) {
            super.drawBar(inside: aRect, flipped: flipped)
            return;
        }
        
        _barRect = aRect
        _flipped = flipped
        
        let beforeKnobRect:NSRect = createBeforeKnobRect()
        let afterKnobRect:NSRect = createAfterKnobRect()
                
        if( self.minValue <= self.doubleValue ) {
            NSDrawThreePartImage(beforeKnobRect, barLeftAgeImage, barFillBeforeKnobImage, barFillBeforeKnobImage,
                                 false, NSCompositingOperation.sourceOver, 1.0, flipped)
        }
        if( self.maxValue >= self.doubleValue ) {
            NSDrawThreePartImage(afterKnobRect, barFillImage, barFillImage, barRightAgeImage,
                                 false, NSCompositingOperation.sourceOver, 1.0, flipped)
        }
    }
    
    func createBeforeKnobRect() -> NSRect {
        if let barRect          = _barRect, 
            let currentKnobRect = _currentKnobRect, 
            let knobImage       = self.knobImage,
            let barFillBeforeKnobImage = self.barFillBeforeKnobImage {
            var beforeKnobRect:NSRect = barRect
            beforeKnobRect.size.width = currentKnobRect.origin.x + knobImage.size.width / 2
            beforeKnobRect.size.height = barFillBeforeKnobImage.size.height
            beforeKnobRect.origin.y = (knobImage.size.height / 2) - (beforeKnobRect.size.height / 2) + 0.5
            return beforeKnobRect;
        }
        return NSRect(x: 0, y: 0, width: 0, height: 0)
    }
    
    func createAfterKnobRect() -> NSRect {
        if let currentKnobRect = _currentKnobRect,
            let knobImage      = self.knobImage,
            let barRect        = _barRect,
            let barFillImage   = self.barFillImage
        {
            var afterKnobRect:NSRect = currentKnobRect;
            
            if (barFillBeforeKnobImage != nil ){
                afterKnobRect.origin.x   += knobImage.size.width / 2
                afterKnobRect.size.width  = barRect.size.width - afterKnobRect.origin.x
                afterKnobRect.size.height = barFillImage.size.height
            }
            else {
                afterKnobRect.origin.x    = knobImage.size.width / 2 
                afterKnobRect.size.width  = barRect.size.width - afterKnobRect.origin.x                
                afterKnobRect.size.height = barRect.size.height
            }
            afterKnobRect.origin.y = (knobImage.size.height / 2) - (afterKnobRect.size.height / 2) + 0.5
            
            return afterKnobRect;
        }
        return NSRect(x: 0, y: 0, width: 0, height: 0)
    }
    
    func setBarFillImage(bFillImage:NSImage) {
        barFillImage = bFillImage;
        if(barFillBeforeKnobImage == nil) {
            barFillBeforeKnobImage = bFillImage;
        }
    }
    
    func setBarFillBeforeKnobImage(bFillBeforeKnobImage:NSImage) {
        barFillBeforeKnobImage = bFillBeforeKnobImage;
        if(barFillImage == nil) {
            barFillImage = bFillBeforeKnobImage;
        }
    }
    
}

public class DHCSlider:NSSlider {
        
    override public var canDraw: Bool {
        return true
    }
    
    public var currentValue:CGFloat = 0
    
    public override func setNeedsDisplay(_ invalidRect: NSRect) {
        super.setNeedsDisplay(invalidRect)
    }
        
    public override func awakeFromNib() {
        super.awakeFromNib()
        if ((self.cell?.isKind(of: DHCSliderCell.self)) == false) {
            let cell:DHCSliderCell = DHCSliderCell()
            self.cell = cell
        }        
        
        (cell as? DHCSliderCell)?.barFillImage?.size.width = frame.width
    }        
    
    public override var frame: NSRect{
        didSet{
            (cell as? DHCSliderCell)?.barFillImage?.size.width = frame.width
        }
    }
    
    public override func sendAction(_ action: Selector?, to target: Any?) -> Bool {
        let ret = super.sendAction(action, to: target)
        needsDisplay = ret
        return ret
    }    
    
    public convenience init(knob:NSImage, 
                            knobAlternate:NSImage? = nil,
                            barFill:NSImage?=nil,
                            barFillBeforeKnob:NSImage?=nil, 
                            barLeftAge:NSImage?=nil, 
                            barRightAge:NSImage?=nil) {
        self.init()
        postsFrameChangedNotifications = true
        _knobAlternateImage = knobAlternate
        self.cell = DHCSliderCell(knob: knob, 
                                  barFill: barFill, 
                                  barFillBeforeKnob: barFillBeforeKnob, 
                                  barLeftAge: barLeftAge, 
                                  barRightAge: barRightAge)
    }
        
    public var doubleClickAction:Selector?
    
    private var _knobAlternateImage:NSImage?

    public override func mouseUp(with event: NSEvent) {
        super.mouseUp(with: event)
    }
    
    private var prevValue:Float = Float.greatestFiniteMagnitude
    public override func mouseDown(with event: NSEvent) {

        if event.clickCount == 0 {
            prevValue = floatValue
        }
        else if event.clickCount > 1 {
            if abs(prevValue-floatValue) > Float.ulpOfOne {
                super.sendAction(doubleClickAction, to: target)
            }
        }
        
        if let prevIm =  knobImage(),
            let im = _knobAlternateImage {
            self.setKnob(image: im)            
            super.mouseDown(with: event)
            self.setKnob(image: prevIm)                
        }
        else {
            super.mouseDown(with: event)
        }
    }
        
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        (cell as? DHCSliderCell)?.barFillImage?.size.width = frameRect.width
    }
    
        
    public func knobImage() -> NSImage? {
        let cell = self.cell as? DHCSliderCell
        return cell?.knobImage
    }
    
    public func setKnob(image:NSImage) {
        let cell = self.cell as? DHCSliderCell
        cell?.knobImage = image
        needsDisplay = true
    }
    
    public func knobAlternateImage() -> NSImage?{
        return _knobAlternateImage
    }
    
    public func setKnobAlternate(image: NSImage) {
        _knobAlternateImage = image
    }
    
    public func barFillImage() -> NSImage? {
        let cell = self.cell as? DHCSliderCell
        return cell?.barFillImage
    }
    
    public func setBarFill(image:NSImage) {
        let cell = self.cell as? DHCSliderCell
        cell?.barFillImage = image
    }
    
    func barFillBeforeKnobImage() -> NSImage? {
        let cell = self.cell as? DHCSliderCell
        return cell?.barFillBeforeKnobImage
    }
    
    func setBarFillBeforeKnob(image:NSImage) {
        let cell = self.cell as? DHCSliderCell
        cell?.barFillBeforeKnobImage = image
    }
    
    func barLeftAgeImage() -> NSImage? {
        let cell = self.cell as? DHCSliderCell
        return cell?.barLeftAgeImage
    }
    
    func setBarLeftAge(image:NSImage) {
        let cell = self.cell as? DHCSliderCell
        cell?.barLeftAgeImage = image
    }
    
    func barRightAgeImage() -> NSImage? {
        let cell = self.cell as? DHCSliderCell
        return cell?.barRightAgeImage
    }
    
    func setBarRightAge(image:NSImage) {
        let cell = self.cell as? DHCSliderCell
        cell?.barRightAgeImage = image
    }
}

