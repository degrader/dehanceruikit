//
//  DHCImageMetaRaw.m
//  DehancerUIKit
//
//  Created by denis svinarchuk on 03.11.17.
//

#import "DHCImageMetaRaw.h"

@implementation DHCImageMetaRaw

@dynamic serial;
@dynamic datetime;

+ (NSString*) name {
    return  @"raw";
}

@end
