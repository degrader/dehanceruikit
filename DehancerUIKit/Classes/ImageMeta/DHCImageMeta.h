//
//  DHCExif.h
//  DehancerUIKit
//
//  Created by denis svinarchuk on 30.10.17.
//

#import <Foundation/Foundation.h>
#import "DHCImageMetaField.h"

typedef NS_ENUM(int, DHCImageMetaState) {
    DHCImageMetaOk        = 0,
    DHCImageMetaXmpOk     = 1,
    DHCImageMetaNotOpened,
    DHCImageMetaProtected,
    DHCImageMetaCorrupted
};

@interface DHCImageMeta : NSObject
- (nonnull instancetype) initWithPath:(nonnull NSString*)path;

- (nonnull instancetype) initWithPath:(nonnull NSString*)path
                              history:(NSInteger)length;

- (nonnull instancetype) initWithPath:(nonnull NSString*)path
                            extension:(nullable NSString*)ext;

- (nonnull instancetype) initWithPath:(nonnull NSString*)path
                            extension:(nullable NSString*)ext
                              history:(NSInteger)length;

@property DHCImageMetaState state;


@property NSInteger         historyLangth;
@property int               error;

- (nullable instancetype) setField:(DHCImageMetaField*_Nonnull)value
                             error:(NSError *_Nullable*_Nullable)error;

- (nullable DHCImageMetaField*) getField:(Class _Nonnull )valueClass
                                 fieldId:(nullable NSString*)fieldId
                                   error:(NSError *_Nullable*_Nullable)error;

- (nullable NSArray*)   getFieldUndoHistory:(Class _Nonnull )valueClass
                                    fieldId:(nullable NSString*)fieldId
                                      error:(NSError *_Nullable*_Nullable)error;

@end
