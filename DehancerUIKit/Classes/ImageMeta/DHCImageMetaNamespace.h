//
//  DHCImageMetaRegistry.hpp
//  DehancerUIKit
//
//  Created by denis svinarchuk on 03.11.17.
//

#ifndef DHCImageMetaRegistry_hpp
#define DHCImageMetaRegistry_hpp

typedef const char * DHC_StringPtr;  // Points to a null terminated UTF-8 string.

const DHC_StringPtr kDHC_CREATOR_TOOL = "Dehancer mLut Maker ML1";
const DHC_StringPtr kDHC_NS_SDK       = "http://dehancer.com/xmp/1.0";
const DHC_StringPtr kDHC_NS_QUAL_TYPE = "type";

@interface DHCImageMetaNamespace : NSObject 
+ (id)shared;
@property (readonly) NSInteger historyLength;
@end


#endif /* DHCImageMetaRegistry_hpp */
