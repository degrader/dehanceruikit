//
//  DHCSplitView.swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 25.09.17.
//

import Cocoa

public protocol DHCSplitViewProtocol{
    var dragControlDidTap:((_ point:NSPoint)->Void)? {get set}
    var behaviorFor:((_ index:Int)->NSSplitViewItem.Behavior)? {get set}            
    var dividerColorFor:((_ index:Int)->NSColor)? {get set}            
    var dividerThicknessFor:((_ index:Int)->CGFloat)? {get set}            
    
    var sideControlNormalImage:NSImage? {get} 
    var sideControlClosedImage:NSImage? {get}   
    var dividerSensitivity:CGFloat {get}
}

open class DHCSplitView: NSSplitView, DHCSplitViewProtocol {
    public var dividerThicknessFor: ((_ index: Int) -> CGFloat)?        
    public var dividerColorFor: ((_ index: Int) -> NSColor)?        
    public var dragControlDidTap:((_ point:NSPoint)->Void)?
    public var behaviorFor:((_ index:Int)->NSSplitViewItem.Behavior)?            
    
    open var sideControlNormalImage:NSImage? {return nil}
    open var sideControlClosedImage:NSImage? {return nil}
    open var dividerSensitivity:CGFloat {return 0}    
    
    override open func layout() {
        needsDisplay = true
        super.layout()
        for v in subviews {
            v.wantsLayer = true
            v.layer?.backgroundColor = NSColor.clear.cgColor
        }
        for (_,v) in arrangedSubviews.enumerated() {
            v.wantsLayer = true
            v.layer?.backgroundColor = NSColor.clear.cgColor            
        }
    }
    
    private lazy var dividersRect:[NSRect?] = [NSRect?](repeating:nil, count: 256)

    private lazy var dividerNormalView:NSImageView? = {
        guard let image = sideControlNormalImage else { return nil}
        return  NSImageView(image: image)
    }()
    
    open override func addSubview(_ view: NSView) {
        super.addSubview(view)
        if let im = dividerNormalView {
            im.removeFromSuperview()
            super.addSubview(im)
            im.frame.origin = NSPoint(x: 100, y: 100)
        }
    }
        
    override open func drawDivider(in rect: NSRect) {                
        isMoved = true
        
        var behavior:NSSplitViewItem.Behavior?
        var index = 0
        for (i,v) in arrangedSubviews.enumerated() {
            let frame = NSInsetRect(v.frame, -dividerThickness, -dividerThickness)
            if frame.contains(rect){
                behavior = behaviorFor?(i)
                index = i
                break
            }            
        }
        
        if let color = dividerColorFor?(index){
            color.set()
        }
        else {
            dividerColor.set()
        }            
        if let thickness = dividerThicknessFor?(index){
            if let _behavior = behavior {
                if _behavior != .sidebar { 
                    var vrect = rect
                    vrect.size.height = thickness
                    vrect.origin.y += dividerThickness
                    let path = NSBezierPath(rect: vrect)  
                    path.fill()
                }
            }
        }
        else {
            rect.fill()                             
        }
    
        if dividersRect.count<index{
            dividersRect.append(contentsOf:  [NSRect?](repeating:nil, count: 256))
        }
        dividersRect[index] = rect
                
        guard let _behavior = behavior else { return }
        if _behavior != .sidebar { return }            
                
        if let im =  drageImageFor(in: rect), let controlRect = dragControlRect(in: rect) { 
            let imageRect = NSMakeRect(0, 0, im.size.width, im.size.height)
            im.draw(in: controlRect, from: imageRect, operation: .sourceOver, fraction: 1, respectFlipped: true, hints: nil)
        }
    }
            
    private func drageImageFor(in rect:NSRect) -> NSImage? {
        var image:NSImage? = nil
        
        if isVertical {
            if rect.origin.x<dividerThickness { image = sideControlClosedImage}
            else { image = sideControlNormalImage }
        }
        else {
            if abs(rect.origin.y-frame.height)<=dividerThickness || rect.origin.y<dividerThickness { image = sideControlClosedImage}
            else { image = sideControlNormalImage }            
        }
        
        return image
    }
    
    private func dragControlRect(in rect: NSRect?) -> NSRect? {
        guard let rect = rect else {return nil}
        
        guard let im = drageImageFor(in: rect) else {return nil}
        
        let imageRect = NSMakeRect(0, 0, im.size.width, im.size.height)        
        var drawingRect = imageRect
        drawingRect.origin = rect.origin
        if isVertical {
            drawingRect.origin.y = rect.height/2-imageRect.size.height/2
        }
        else {
            drawingRect.origin.x = rect.width/2-imageRect.size.width/2
            //drawingRect.origin.y += dividerThickness //imageRect.size.height * 2 //- imageRect.size.height
        }
        return drawingRect
    }           
    
    lazy var trackingArea:NSTrackingArea? = nil
    
    override open func updateTrackingAreas() {
        super.updateTrackingAreas()
        if let t = trackingArea{
            removeTrackingArea(t)
        }
        trackingArea = NSTrackingArea(rect: frame,
                                      options: [.mouseEnteredAndExited, .activeAlways, .mouseMoved],
                                      owner: self, userInfo: nil)
        addTrackingArea(trackingArea!)
    }
    
    private var currentCursor:NSCursor?
    
    private func indexOfDivider(with event: NSEvent) -> Int? {
        let loc = convert(event.locationInWindow, from: window?.contentView)
        for (i,_rect) in dividersRect.enumerated() {
            guard var rect = _rect else { continue } 
            rect = NSInsetRect(rect, -dividerSensitivity, -dividerSensitivity)
            if NSPointInRect(loc, rect) {
                return i
            }
        }
        return nil
    }
    
    private func cursorOnDragControl(with event: NSEvent, index dividerIndex: Int) -> Bool{
        let loc = convert(event.locationInWindow, from: window?.contentView)
        let behavior = behaviorFor?(dividerIndex)
        if behavior != .sidebar { return false } 
        guard let rect = dragControlRect(in: dividersRect[dividerIndex]) else {return false}
        return NSPointInRect(loc, rect)        
    }
    
    private func cursorOnDivider(with event: NSEvent, index dividerIndex: Int) -> Bool {
        let loc = convert(event.locationInWindow, from: window?.contentView)
        guard var rect = dividersRect[dividerIndex] else {return false}
        //rect = NSInsetRect(rect, -dividerSensitivity, -dividerSensitivity)
        rect = NSInsetRect(rect, -1, -1)
        return NSPointInRect(loc, rect)        
    }
    
    override open func mouseDown(with event: NSEvent) {
        isMoved = false
        super.mouseDown(with: event)
        if let dividerIndex = indexOfDivider(with: event) {
            if cursorOnDragControl(with:event, index: dividerIndex) && !isMoved {
                dragControlDidTap?(convert(event.locationInWindow, from: window?.contentView))
            }
        }
    }  
        
    
    private var isMoved = false
    override open func mouseMoved(with event: NSEvent) {
        isMoved = true
                        
        if let dividerIndex = indexOfDivider(with: event) {
            
            if cursorOnDragControl(with:event, index: dividerIndex) {
                NSCursor.pointingHand.set()
            }
            else {
                if cursorOnDivider(with: event, index: dividerIndex) {
                    currentCursor = isVertical ? NSCursor.resizeLeftRight : NSCursor.resizeUpDown
                }
                else {
                    currentCursor = NSCursor.arrow
                }
                currentCursor?.set()
            }
        }  
        else {
            super.mouseMoved(with: event)
        }
    }
    
    override  open func mouseUp(with event: NSEvent) {
        isMoved = false
        currentCursor?.set()
        super.mouseUp(with: event)
    }
    
    override  open func mouseExited(with event: NSEvent) {
        currentCursor = NSCursor.arrow
        currentCursor?.set()
    }
    
    override  open func mouseEntered(with event: NSEvent) {
        currentCursor = NSCursor.current
    }        
}

