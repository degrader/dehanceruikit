//
//  EDStarRating.
//
//  Created by Ernesto Garcia on 26/02/12.
//  2013 cocoawithchurros.com
//  Distributed under MIT license
//
//  Version 1.1

// 
// Sources: https://github.com/erndev/EDStarRating
//

#import <Availability.h>
#ifdef __MAC_OS_X_VERSION_MAX_ALLOWED
#define EDSTAR_MACOSX 1
#define EDSTAR_IOS    0
#else
#define EDSTAR_MACOSX 0
#define EDSTAR_IOS    1
#endif



#if EDSTAR_MACOSX
#import <Cocoa/Cocoa.h>
#else
#import <UIKit/UIKit.h>
#endif


typedef NS_ENUM(uint, DHCStarRatingMode) {
    DHCStarRatingFull=0,
    DHCStarRatingHalf,
    DHCStarRatingAccurate
};

//typedef NSUInteger EDStarRatingDisplayMode;
typedef void(^DHCStarRatingReturnBlock)(float rating);
@protocol DHCStarRatingProtocol;

#if EDSTAR_MACOSX
#define __DHCControl   NSControl
#else
#define __DHCControl   UIControl
typedef UIColor     NSColor;
typedef UIImage     NSImage;

#endif

@interface DHCStarRating : __DHCControl

#if EDSTAR_MACOSX
@property (nonatomic,strong) NSColor *backgroundColor;
#endif
@property (nonatomic,strong) NSImage *backgroundImage;
@property (nonatomic,strong) NSImage *starHighlightedImage;
@property (nonatomic,strong) NSImage *starImage;
@property (nonatomic) NSInteger maxRating;
@property (nonatomic) float rating;
@property (nonatomic) CGFloat horizontalMargin;
@property (nonatomic) BOOL editable;
@property (nonatomic) DHCStarRatingMode displayMode;
@property (nonatomic) float halfStarThreshold;

@property (nonatomic,weak) id<DHCStarRatingProtocol> delegate;
@property (nonatomic,copy) DHCStarRatingReturnBlock returnBlock;
@end


@protocol DHCStarRatingProtocol <NSObject>

@optional
-(void)starRatingChanged:(DHCStarRating*)control rating:(float)rating;

@end

