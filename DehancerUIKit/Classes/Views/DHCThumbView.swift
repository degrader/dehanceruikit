//
//  DHCThumbView.swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 27.09.17.
//

import Cocoa

public class DHCThumbView: NSImageView {
    
    override public init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        configure()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    
    override public var image: NSImage?{
        didSet{
            layer?.contents = image
        }
    }
    
    public func configure()  {
        layer = CALayer()
        layer?.contentsGravity = kCAGravityResizeAspect
        wantsLayer = true
        imageAlignment = .alignCenter
        //imageFrameStyle = .none
        //imageScaling = .scaleProportionallyDown
        //layer?.masksToBounds = true
    }
    
}
