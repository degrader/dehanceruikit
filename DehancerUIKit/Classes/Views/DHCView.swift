//
//  DHCView.swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 22.09.17.
//  Copyright © 2017 Dehancer. All rights reserved.
//

import Cocoa

@IBDesignable open class DHCView: NSView {
    
    @IBInspectable public var backgroundColor: NSColor? {didSet{ needsDisplay = true } }    
    @IBInspectable open var cornerRadius: CGFloat = 0 { didSet{ needsDisplay = true } }
    @IBInspectable open var borderWidth: CGFloat  = 0 { didSet{ needsDisplay = true } }
    @IBInspectable open var borderColor: NSColor?     { didSet{ needsDisplay = true } }  
    
    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        configure()
    }
    
    public required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        configure()
    }
    
    open func configure(){}
    
    open override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        self.wantsLayer             = true
        self.layer?.cornerRadius    = cornerRadius
        self.layer?.borderColor     = borderColor?.cgColor
        self.layer?.borderWidth     = borderWidth
        self.layer?.backgroundColor = backgroundColor?.cgColor
        self.layer?.masksToBounds = false                        
    } 
}
