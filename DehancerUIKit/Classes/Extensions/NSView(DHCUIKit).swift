//
//  NSView(DHCUIKit).swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 17.10.17.
//

import Cocoa

public extension NSImageView {
    public func start(){
        animates = true
    }
    
    public func stop(){
        animates = false
    }
}

public extension NSView {
    public var snapshot: NSImage {
        guard let bitmapRep = bitmapImageRepForCachingDisplay(in: bounds) else { return NSImage() }
        cacheDisplay(in: bounds, to: bitmapRep)
        let image = NSImage()
        image.addRepresentation(bitmapRep)
        bitmapRep.size = bounds.size.screenScale()
        return image
    }
    
    public func drawString(string:String, inRect rect: NSRect,
                           alignment:NSTextAlignment = .center,
                           lineBreakMode:NSParagraphStyle.LineBreakMode = .byTruncatingTail,
                           color:NSColor = NSColor.white,
                           font:NSFont = NSFont.labelFont(ofSize: 12)
        
        ) {
        
        let style = NSMutableParagraphStyle()
        style.lineBreakMode = lineBreakMode
        style.alignment = alignment
        
        var rect = rect
        
        let fontSize = string.size(withAttributes: [NSAttributedStringKey.font : font])
        
        rect.size.height = fontSize.height
        (string  as NSString).draw(in: rect, withAttributes: [NSAttributedStringKey.paragraphStyle : style,
                                                              NSAttributedStringKey.foregroundColor: color,
                                                              NSAttributedStringKey.font: font])
    }

}

public extension CGSize {
    public func screenScale() -> CGSize {
        let scale = NSScreen.main?.backingScaleFactor ?? 1
        return CGSize(width: width * scale, height: height * scale)
    }
}
