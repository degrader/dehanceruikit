//
//  NSDate.m
//  DehancerUIKit
//
//  Created by denis svinarchuk on 14.10.17.
//

#import "NSDate(DHCUIKit).h"

#include <assert.h>
#include <CoreServices/CoreServices.h>
#include <mach/mach.h>
#include <mach/mach_time.h>
#include <unistd.h>

#if MAC_OS_X_VERSION_MAX_ALLOWED == MAC_OS_X_VERSION_10_5
@interface NSProcessInfo (SnowLeopard)
- (NSTimeInterval)systemUptime;
@end

@interface NSDate (SnowLeopard)
- (id)dateByAddingTimeInterval:(NSTimeInterval)seconds;
@end
#endif


// Boosted from Apple sample code
uint64_t UpTimeInNanoseconds(void)
{
    uint64_t        time;
    uint64_t        timeNano;
    static mach_timebase_info_data_t    sTimebaseInfo;
    
    time = mach_absolute_time();
    
    // Convert to nanoseconds.
    
    // If this is the first time we've run, get the timebase.
    // We can use denom == 0 to indicate that sTimebaseInfo is
    // uninitialised because it makes no sense to have a zero
    // denominator is a fraction.
    
    if ( sTimebaseInfo.denom == 0 ) {
        (void) mach_timebase_info(&sTimebaseInfo);
    }
    
    // Do the maths.  We hope that the multiplication doesn't
    // overflow; the price you pay for working in fixed point.
    
    timeNano = time * sTimebaseInfo.numer / sTimebaseInfo.denom;
    
    return timeNano;
}


@implementation NSDate (NSDate_Additions)

+(NSTimeInterval) timeIntervalSinceSystemStartup
{
    return [[NSProcessInfo processInfo] systemUptime];
}

-(NSTimeInterval) timeIntervalSinceSystemStartup
{
    return( [self timeIntervalSinceDate:[NSDate dateOfSystemStartup]] );
}

+(NSDate *) dateOfSystemStartup
{
    return( [NSDate dateWithTimeIntervalSinceNow:-([NSDate timeIntervalSinceSystemStartup])] );
}

+(NSDate *) dateWithCGEventTimestamp:(CGEventTimestamp)cgTimestamp
{
    NSDate *ssuDate = nil;
    NSDate *cgDate = nil;
    
    ssuDate = [NSDate dateOfSystemStartup];
    
    cgDate = [ssuDate dateByAddingTimeInterval:(cgTimestamp/1000000000.0)];
    
    return cgDate ;
}

@end
