//
//  NSImage(DHCUIKit).swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 01.10.17.
//

import Cocoa
import ImageIO

public extension NSImage {

    public func flip(direction:Int = 0) -> NSImage {
        
        let existingImage: NSImage? = self
        let existingSize: NSSize? = existingImage?.size
        let newSize: NSSize? = NSMakeSize((existingSize?.width)!, (existingSize?.height)!)
        let flipedImage = NSImage(size: newSize!)
        flipedImage.lockFocus()
        
        let t = NSAffineTransform.init()
        if direction == 0 {
            t.translateX(by: (existingSize?.width)!, yBy: 0.0)
            t.scaleX(by: -1.0, yBy: 1.0)
            t.concat()
        }
        else {
            t.translateX(by: 0.0, yBy: (existingSize?.height)!)
            t.scaleX(by: 1.0, yBy: -1.0)
            t.concat()            
        }
        
        let rect:NSRect = NSMakeRect(0, 0, (newSize?.width)!, (newSize?.height)!)
        existingImage?.draw(at: NSZeroPoint, from: rect, operation: .sourceOver, fraction: 1.0)
        flipedImage.unlockFocus()
        return flipedImage
    }
    
    convenience init(color: NSColor, size: NSSize) {
        self.init(size: size)
        lockFocus()
        color.setFill()
        color.drawSwatch(in: NSMakeRect(0, 0, size.width, size.height))
        unlockFocus()
    }
    
    public func resize(factor level: CGFloat) -> NSImage {
        let _image = self
        let newRect: NSRect = NSMakeRect(0, 0, _image.size.width, _image.size.height)
        
        let imageSizeH: CGFloat = _image.size.height * level
        let imageSizeW: CGFloat = _image.size.width * level
        
        let newImage = NSImage(size: NSMakeSize(imageSizeW, imageSizeH))
        newImage.lockFocus()
        NSGraphicsContext.current?.imageInterpolation = NSImageInterpolation.medium
        
        _image.draw(in: NSMakeRect(0, 0, imageSizeW, imageSizeH), from: newRect, operation: .sourceOver, fraction: 1)
        newImage.unlockFocus()
        
        return newImage
    }
    
    public static var typeExtensions:[String] {
        return NSImage.imageTypes.map { (name) -> String in
            return name.components(separatedBy: ".").last!
        }
    }
    
    public class func getTypeIdentifierHint(contentsOf url: URL) -> String? {
        guard let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) else { return nil }
        return CGImageSourceGetType(imageSource) as String?
    }
    
    public class func getMeta(contentsOf url: URL) -> CFDictionary? {
        guard let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) else { return nil }
        return CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil)
    }
    
    public class func getSize(contentsOf url: URL) -> NSSize? {
        guard let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) else { return nil }
        return NSImage.getSize(imageSource: imageSource)
    }
    
    public class func getSize(imageSource: CGImageSource) -> NSSize? {              
        guard let properties =  CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as? [String: AnyObject] else { return nil }
        if let w = properties[kCGImagePropertyPixelWidth as String]?.floatValue,
            let h = properties[kCGImagePropertyPixelHeight as String]?.floatValue{
            
            if let orientation = properties[kCGImagePropertyOrientation as String]?.integerValue{
                if [3,6,8].contains(orientation) {
                    return NSSize(width: CGFloat(h), height: CGFloat(w))   
                }                
            }
            
            return NSSize(width: CGFloat(w), height: CGFloat(h))
        }
        return nil
    }
    
    public class func preview(contentsOf url: URL, maxSize: CGFloat, fromImageAlways:Bool) -> NSImage? {
        guard let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) else { return nil }
        guard let size = NSImage.getSize(imageSource: imageSource) else {return nil}
        
        var options = [
            //kCGImageSourceShouldAllowFloat as String: true as NSNumber,
            kCGImageSourceShouldCache as String: true as NSNumber,
            //kCGImageSourceCreateThumbnailFromImageAlways as String: fromImageAlways as NSNumber,
            kCGImageSourceCreateThumbnailWithTransform as String: true as NSNumber,
            //kCGImageSourceCreateThumbnailFromImageIfAbsent as String: false as NSNumber,
            kCGImageSourceThumbnailMaxPixelSize as String: maxSize as NSNumber
            ]// as CFDictionary
        
        if fromImageAlways {
            options[kCGImageSourceCreateThumbnailFromImageAlways as String] = fromImageAlways as NSNumber
        }
        else {
            options[kCGImageSourceCreateThumbnailFromImageIfAbsent as String] = true as NSNumber
        }
        
        guard let thumbnail = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options as CFDictionary) else { return nil }
        let maxx  = max(CGFloat(size.height),CGFloat(size.width))
        var scale = maxSize/maxx
        scale = scale > 1 ? 1 : scale 
        let width = size.width * scale
        let height = size.height * scale 
        
        return NSImage(cgImage: thumbnail, size: NSSize(width: width, height: height))
    }

    public class func thumbNail(contentsOf url: URL, maxSize: CGFloat) -> NSImage? {
        guard let thumbnail = cgImage(contentsOf: url, maxSize: maxSize)  else { return nil}         
        return NSImage(cgImage: thumbnail, size: NSSize(width: maxSize, height: maxSize))
    }
    
    public class func cgImage(contentsOf url: URL, maxSize: CGFloat? = nil) -> CGImage? {
        guard let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) else { return nil }
        guard let size = NSImage.getSize(imageSource: imageSource) else {return nil}
        let ms = maxSize ?? max(size.width,size.height);
        
        let options = [
            kCGImageSourceShouldAllowFloat as String: true as NSNumber,
            kCGImageSourceCreateThumbnailWithTransform as String: false as NSNumber,
            kCGImageSourceCreateThumbnailFromImageIfAbsent as String: true as NSNumber,
            kCGImageSourceThumbnailMaxPixelSize as String: ms as NSNumber
            ] as CFDictionary
        
        return CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options)
    }
}
