//
//  NSCell.swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 07.11.17.
//

import Cocoa

public extension NSCell {
    public func drawString(string:String, inRect rect: NSRect,
                           alignment:NSTextAlignment = .center,
                           lineBreakMode:NSParagraphStyle.LineBreakMode = .byTruncatingTail,
                           color:NSColor = NSColor.white,
                           font:NSFont = NSFont.labelFont(ofSize: 12)
        
        ) {
        
        let style = NSMutableParagraphStyle()
        style.lineBreakMode = lineBreakMode
        style.alignment = alignment
        
        var rect = rect
        
        let fontSize = string.size(withAttributes: [NSAttributedStringKey.font : font])
        
        rect.size.height = fontSize.height
        (string  as NSString).draw(in: rect, withAttributes: [NSAttributedStringKey.paragraphStyle : style,
                                                              NSAttributedStringKey.foregroundColor: color,
                                                              NSAttributedStringKey.font: font])
    }
}
