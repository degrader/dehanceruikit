//
//  NSSplitView(Restoration).swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 22.09.17.
//

import Cocoa

public extension NSSplitView {
    
    /*
     ** unfortunately this needs to be called in the controller's viewDidAppear function as
     ** auto layout kicks in to override any default values after the split view's awakeFromNib
     */
        
    public func restoreAutoSavePositions(initilize:(()->Void)?=nil) {
                
        guard let name = self.autosaveName else {return}
        
        let key = "NSSplitView Subview Frames \(name)"
        
        let subViewFrames = UserDefaults.standard.array(forKey: key)
        
        guard subViewFrames != nil else {
            initilize?()
            return             
        }
        
        for (i, frame) in (subViewFrames?.enumerated())! {
            
            if let frameString = frame as? String {
                
                let components = frameString.components(separatedBy: ", ")
                guard components.count >= 4 else { return }
                
                var position: CGFloat = 0.0
                
                // Manage the 'hidden state' per view
                let hidden = NSString(string:components[4].lowercased()).boolValue
                let subView = self.subviews[i]
                subView.isHidden = hidden

                NSLog("2. restoreAutoSavePositions == \(subView)")

                // Set height (horizontal) or width (vertical)
                if self.isVertical {
                    if let n = NumberFormatter().number(from: components[2]) {
                        position = CGFloat(truncating: n)
                    }
                } else {
                    if let n = NumberFormatter().number(from: components[3]) {
                        position = CGFloat(truncating: n)
                    }
                }
                
                setPosition(position, ofDividerAt: i)
            }
        }
    }
}
