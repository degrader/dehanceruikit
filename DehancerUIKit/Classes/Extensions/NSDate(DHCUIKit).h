//
//  NSDate.h
//  DehancerUIKit
//
//  Created by denis svinarchuk on 14.10.17.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>
@interface NSDate (NSDate_Additions)

+(NSTimeInterval) timeIntervalSinceSystemStartup;
-(NSTimeInterval) timeIntervalSinceSystemStartup;
+(NSDate *) dateOfSystemStartup;
+(NSDate *) dateWithCGEventTimestamp:(CGEventTimestamp)cgTimestamp;

@end

