//
//  SlidersController.swift
//  DehancerUIKit
//
//  Created by denis svinarchuk on 22.09.17.
//  Copyright © 2017 Dehancer. All rights reserved.
//

import Cocoa

class SlidersController: NSViewController, DHCRangeSliderDelegate {

    @IBOutlet weak var rangeSlider: DHCRangeSlider!
    @IBAction func didChangeValue(_ sender: DHCRangeSlider) {
        Swift.print(" value = \(sender.startValue)")
    }
    
    func rangeSlider(_ control: DHCRangeSlider, didChange start: CGFloat, end: CGFloat) {
        Swift.print(" delegate value = \(start)")

    }
    
    func rangeSlider(_ control: DHCRangeSlider, didEndChanging start: CGFloat, end: CGFloat) {
        Swift.print(" delegate value end = \(start)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rangeSlider.delegate = self
        // Do view setup here.
    }
    
}
