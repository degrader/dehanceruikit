Pod::Spec.new do |s|
    
    s.name         = 'DehancerUIKit'
    s.version      = '0.2.0'
    s.license      = { :type => 'MIT', :file => 'LICENSE' }
    s.author       = { 'Denn Never' => 'https://imagemetalling.wordpress.com/' }
    s.homepage     = 'http://www.dehancer.com/'
    s.summary      = 'Dehancer UI Kit'
    s.description  = 'Dehancer UI Kit'
    
    s.source       = { :git => 'https://bitbucket.org/degrader/mbdoubleslider', :tag => s.version}
    
    s.osx.deployment_target = "10.12"
    
    s.exclude_files = 'DehancerUIKit/Classes/HotKey/Example/**/*.*','DehancerUIKit/Classes/HotKey/Tests/**/*.*'
    s.source_files  = 'DehancerUIKit/Classes/**/*.{h,swift,m,hpp,cpp,hh,mm}', 'DehancerUIKit/Classes/*.{h,swift,m,cpp,hpp,hh,mm}'
    s.public_header_files = 'DehancerUIKit/Classes/**/*{h,hpp}', 'DehancerUIKit/Classes/*.{h,hpp}'
    
    s.libraries =  'z', 'expat', 'XMPCoreStatic', 'XMPFilesStatic'

    s.header_dir   = 'DehancerUIKit'

    s.xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => 'MAC_ENV=1', 'HEADER_SEARCH_PATHS' => '/usr/local/include/XMPToolkit', 'LIBRARY_SEARCH_PATHS' => '/usr/local/lib/XMPToolkit'}

    s.requires_arc = true

    #, 'vendor/libxmptoolkit/include/*.{h,hpp,incl_cpp}', 'vendor/libxmptoolkit/include/**/*.{h,hpp,incl_cpp}'    
    #s.vendored_libraries  = 'vendor/libfswatch/lib/libfswatch.a'
    #s.xcconfig = { 'HEADER_SEARCH_PATHS' => '/usr/local/include/libfswatch/c /usr/local/include/libfswatch/c++'}
    
end
